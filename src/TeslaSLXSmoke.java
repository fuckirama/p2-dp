public class TeslaSLXSmoke extends Car{

	TeslaSLXFactory carFactory;
	

	
	public TeslaSLXSmoke(TeslaSLXFactory carFactory){
		
		this.carFactory = carFactory;
		
	}


	
	void makeCar() {
		
		System.out.println("Making" + "" +  getName());

		
		hp = carFactory.addHP();
		speed = carFactory.addSpeed();
		
	}
	
}