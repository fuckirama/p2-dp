public abstract class CarBuilding {
	
	protected abstract Car makeTeslaSLX(String typeOfCar);
 	
	public Car orderTheCar(String typeOfCar) {
		Car theCar = makeTeslaSLX(typeOfCar);
		
		theCar.makeCar();
		theCar.displayTheCar();
		theCar.HorsePowerOfCar();
		
		return theCar;		
	}
}