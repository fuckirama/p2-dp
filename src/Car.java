public abstract class Car {
	
	private String name;
	
	HP hp;
	Speed speed;
	
	public String getName() { return name; }
	public void setName(String newName) { name = newName; }
	
	abstract void makeCar();
	public void displayTheCar(){
		System.out.println(getName() + " is on the screen");		
	}
	
	public void HorsePowerOfCar(){		
		System.out.println(getName() + " has " + hp + "hp");		
	}
	
	public String toString(){		
		String infoOnCar = "The " + name + " has a top speed of " + speed + 
				" and horse power of " + hp;		
		return infoOnCar;		
	}	
}