public class TeslaSLXSmokeBuilding extends CarBuilding {

	protected Car makeTeslaSLX(String typeOfCar) {
		Car theCar = null;
		
		
		if(typeOfCar.equals("Smoke")){
			TeslaSLXFactory carPartsFactory = new TeslaSLXSmokeFactory();
			theCar = new TeslaSLXSmoke(carPartsFactory);
			theCar.setName("Tesla SLX Smoke");
			
		} else 

			
		if(typeOfCar.equals("Tornado")){
			TeslaSLXFactory carPartsFactory = new TeslaSLXTornadoFactory();
			theCar = new TeslaSLXTornado(carPartsFactory);
			theCar.setName("TeslaSLX Tornado");
			
		} 
		
		return theCar;
	}
}